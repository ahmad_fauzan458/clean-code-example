from datetime import datetime

from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permission import isAuthenticated
from rest_framework.response import Response
from rest_franework.status import (HTTP_400_BAD_REQUEST, HTTP_200_OK, HTTP_403_FORBIDDEN)

from .models import User, Pengumuman, MataKuliah, JenisPengumuman, \
    Ruang, Sesi, StatusPengumuman

from .serializers import PengumumanSerializer

def filter_pengumuman(request):
    pengumuman_request = request.GET["tanggal"]
    try:
        pengumuman_date = datetime.strptime(
            pengumuman_request, '%d-%m-%Y').date()
    except ValueError as err:
        return Response({
            'detail': 'Pengumuman does not exist.'
        }, status=400)

    if request.user.user_type == User.ADMIN:
        filter_date = Pengumuman.all_objects.filter(
            tanggal_kelas__date=pengumuman_date)
    else:
        filter_date = Pengumuman.objects.filter(
            tanggal_kelas__date=pengumuman_date)
    pengumuman_response = (PengumumanSerializer(x).data for x in filter_date)
    return Response({
        "pengumuman_response": pengumuman_response,
    }, status=200)


def edit_pengumuman(request, key):
    try:
        pengumuman = Pengumuman.objects.get(pk=key)
    except Pengumuman.DoesNotExist:
        return Response(PENGUMUMAN_DOESNT_EXIST_ERROR_MSG, status=HTTP_STATUS_400)

    if request.user.user_type != User.ADMIN and \
            pengumuman.pembuat != request.user:
        return Response({
            'detail': 'Not enough privileges.'
        }, status=HTTP_STATUS_403)

    pengumuman.nama_dosen = request.data.get('nama_dosen')
    pengumuman.nama_asisten = request.data.get('nama_asisten')
    pengumuman.komentar = request.data.get('komentar')

    try:
        pengumuman.tanggal_kelas = \
            datetime.strptime(request.data.get('tanggal_kelas'), '%Y-%m-%d')
        pengumuman.nama_mata_kuliah = \
            MataKuliah.objects.get(nama=request.data.get('nama_mata_kuliah'))
        pengumuman.jenis_pengumuman = \
            JenisPengumuman.objects.get(
                nama=request.data.get('jenis_pengumuman'))
        pengumuman.nama_ruang = \
            Ruang.objects.get(nama=request.data.get('nama_ruang'))
        pengumuman.nama_sesi = \
            Sesi.objects.get(nama=request.data.get('nama_sesi'))
        pengumuman.nama_status_pengumuman = \
            StatusPengumuman.objects.get(
                nama=request.data.get('nama_status_pengumuman'))
    except (ObjectDoesNotExist, ValueError, TypeError):
        return Response({
            'detail': 'Invalid data.'
        }, status=HTTP_STATUS_400)

    pengumuman.save()

    return Response({
        "success": True,
        "pengumuman": PengumumanSerializer(pengumuman).data
    }, status=HTTP_STATUS_200)

