from datetime import datetime
from rest_framework.decorators import api_view, permission_classes
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from rest_framework.status import (HTTP_400_BAD_REQUEST, HTTP_200_OK, HTTP_403_FORBIDDEN)

from .models import User, Pengumuman, MataKuliah, JenisPengumuman, Ruang, Sesi, StatusPengumuman
from .serializers import PengumumanSerializer

def filter_pengumuman(request):
    #Take parameter "tanggal" from GET request
    pengumuman_request = request.GET["tanggal"]
    #Try block for error handling
    try:
        #Cast string formatted date to datetime
        pengumuman_date = datetime.strptime(pengumuman_request, '%d-%m-%Y').date()
    #Catch value error
    except ValueError as err:
        #Return response if value error raises
        return Response({
            #The error message
            'detail': 'Pengumuman does not exist.'
            #With request 400
        }, status=400)

    # if user is admin, return all include soft delete
    if request.user.user_type == User.ADMIN:
        #Filter data for admin
        filter_date = Pengumuman.all_objects.filter(tanggal_kelas__date=pengumuman_date)
    else:
        #filter data for other than admin
        filter_date = Pengumuman.objects.filter(tanggal_kelas__date=pengumuman_date)
    #create response by serialize every pengumuman
    pengumuman_response = (PengumumanSerializer(x).data for x in filter_date)
    #return the response that serialized before with status 200
    return Response({"pengumuman_response": pengumuman_response, "message":}, status=200)


def edit_pengumuman(request, key):
    #Try block
    try:
        #Try get pengumuman by pk
        pengumuman = Pengumuman.objects.get(pk=key)
    #Except if pengumuman does not exist
    except Pengumuman.DoesNotExist:
        #Return the response
        return Response({
            #The response message
            'detail': 'Pengumuman does not exist.'
            #with status
        }, status=400)

    #Check if request user != Admin and pengumuan pembuat != request user
    if request.user.user_type != User.ADMIN and pengumuman.pembuat != request.user:
        return Response({
            'detail': 'Not enough privileges.'
        }, status=403)

    #Assign value for pengumuman nama dosen
    pengumuman.nama_dosen = request.data.get('nama_dosen')
    # Assign value for pengumuman nama asisten
    pengumuman.nama_asisten = request.data.get('nama_asisten')
    # Assign value for pengumuman komentar
    pengumuman.komentar = request.data.get('komentar')

    #try block
    try:
        # Assign value for pengumuman tanggal kelas
        pengumuman.tanggal_kelas = datetime.strptime(request.data.get('tanggal_kelas'),'%Y-%m-%d')
        # Assign value for pengumuman mata kuliah
        pengumuman.nama_mata_kuliah = MataKuliah.objects.get(nama=request.data.get('nama_mata_kuliah'))
        # Assign value for jenis pengumuman
        pengumuman.jenis_pengumuman = JenisPengumuman.objects.get(nama=request.data.get('jenis_pengumuman'))
        # Assign value for pengumuman nama ruang
        pengumuman.nama_ruang = Ruang.objects.get(nama=request.data.get('nama_ruang'))
        # Assign value for pengumuman nama sesi
        pengumuman.nama_sesi = Sesi.objects.get(nama=request.data.get('nama_sesi'))
        # Assign value for pengumuman nama status
        pengumuman.nama_status_pengumuman = StatusPengumuman.objects.get(nama=request.data.get('nama_status_pengumuman'))
    # except block
    except (ObjectDoesNotExist, ValueError, TypeError):
        #return the response
        return Response({
            #the message
            'detail': 'Invalid data.'
        #with status
        }, status=400)

    #save the pengumuman
    pengumuman.save()

    # return the response
    return Response({
        # the message
        "success": True,
        "pengumuman": PengumumanSerializer(pengumuman).data
        #with status
    }, status=200)

